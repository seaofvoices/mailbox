[![pipeline status](https://gitlab.com/seaofvoices/mailbox/badges/master/pipeline.svg)](https://gitlab.com/seaofvoices/mailbox/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue)](LICENSE.txt)

# mailbox

mailbox allows to save player data, and keep track of the different saves through time. It is made to work as a [crosswalk](https://crosswalk.seaofvoices.ca) module.

# [Documentation](https://mailbox.seaofvoices.ca)

Check out the guide and API reference on the [documentation site](https://mailbox.seaofvoices.ca).

# License

mailbox is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for details.
