# Installation

mailbox is made to work as a [crosswalk](https://seaofvoices.gitlab.io/crosswalk/) module. All you need to do to use mailbox is to put it inside the **server modules** folder where crosswalk expects them. Then, you can start using mailbox [functions](../api.md) through any other crosswalk server modules.


# Roblox Model

You can download the Roblox model file and insert it directly into your game.

| version | asset |
| :--: | :--: |
| master | [mailbox.rbxm](../releases/master/mailbox.rbxm) |


# As a Git Submodule

To add the project as a git submodule into an existing git repository, run:

```
git submodule add https://gitlab.com/seaofvoices/mailbox.git modules/mailbox
```

This will insert it in a folder named `modules` under the root of the repository, but you can put it anywhere you want. Then, you can use a tool like [Rojo](https://rojo.space/) to sync the code into your place.
