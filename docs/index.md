# Home

Mailbox is a [crosswalk](https://seaofvoices.gitlab.io/crosswalk/) server module that automatically saves and loads player data for you.

It is designed to avoid a centralized data module with all the game information of a player. Instead, each server modules can register their own data.

Mailbox is easy to use, it has a really minimal API. Find the functions available to use in the [API reference page](api.md).

!!! Warning
    This documentation is work in progress, if you have found typos or if you have any suggestion on what could be added, open a [issue on Gitlab](https://gitlab.com/seaofvoices/mailbox/-/issues/new).
