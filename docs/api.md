# API

## Open
```
Open(gameDataStoreName: string, playerSaveStoreName: string)
```

This function must be called only once. It will initialize the two datastores of `mailbox`.

## RegisterDataHandler
```
RegisterDataHandler(
    name: string,
    onLoaded: function,
    onRemoved: function,
    getDefault: function
)
```

Creates a new data handler.
