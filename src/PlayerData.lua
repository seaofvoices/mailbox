local Result = require(script.Parent:WaitForChild('Result'))
local utils = require(script.Parent:WaitForChild('utils'))
local ErrorKind = utils.ErrorKind
local fromJson = utils.fromJson
local toJson = utils.toJson

local PlayerData = {}

function PlayerData:saveGame(gameId)
    local newSaves = self:_createNewSaves(gameId)
    local encodedSaves = toJson(newSaves):expect('saves should be json encodable')
    local result = self.saveRequest(encodedSaves)

    if result:isOk() then
        self.sessionGameId = gameId
        self.saves = newSaves
    end

    return result
end

function PlayerData:loadLatestGame()
    local gameId = self:_getLatestGameId()

    if not gameId then
        self.gamePersistence:create(self.player)
        return Result.ok()
    end

    local result = self.gamePersistence:load(gameId, self.player)

    while result:isError() and self.player.Parent do
        local errorValue = result:unwrapError()

        if not errorValue:is(ErrorKind.GetRequestFailed) then
            -- return here so it only retries when it fails to get
            -- the game data
            return result
        end
        wait(1)
        result = self.gamePersistence:load(gameId, self.player)
    end

    return result
end

function PlayerData:connectToSaveRequest(callback)
    assert(self.saveRequest == nil, 'attempt to connect multiple callbacks')
    self.saveRequest = callback
end

function PlayerData:getSessionGameId()
    return self.sessionGameId
end

function PlayerData:_createNewSaves(gameId)
    local newSaves = {
        {
            Time = os.time(),
            DataId = gameId,
        }
    }
    for i = 1, #self.saves do
        newSaves[i + 1] = self.saves[i]
    end
    return newSaves
end

function PlayerData:_getLatestGameId()
    local firstSave = self.saves[1]
    if firstSave then
        return firstSave.DataId
    else
        return nil
    end
end

local PlayerDataMetatable = { __index = PlayerData }

local function create(saves, player, gamePersistence)
    return setmetatable({
        saves = saves,
        player = player,
        gamePersistence = gamePersistence,
        sessionGameId = nil,
        saveRequest = nil,
    }, PlayerDataMetatable)
end

return {
    new = function(player, gamePersistence)
        return create({}, player, gamePersistence)
    end,
    deserialize = function(encodedData, player, gamePersistence)
        local result = fromJson(encodedData)

        return result:map(function(decodedJson)
            return create(decodedJson, player, gamePersistence)
        end)
    end,
}
