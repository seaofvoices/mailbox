local PlayerData = require(script.Parent:WaitForChild('PlayerData'))
local utils = require(script.Parent:WaitForChild('utils'))
local Error = utils.Error
local Result = require(script.Parent:WaitForChild('Result'))

local PlayerPersistence = {}

function PlayerPersistence:load(player)
    local id = tostring(player.UserId)

    local getResult = Result.new(function()
        return self.datastore:GetAsync(id)
    end):mapError(Error.getRequestFailed)

    local playerDataResult = getResult:andThen(function(jsonData)
        if jsonData == nil then
            return Result.ok(PlayerData.new(player, self.gamePersistence))
        end

        return PlayerData.deserialize(jsonData, player, self.gamePersistence)
            :orElse(function(errorValue)
                warn(('unable to decode player %q data: %s'):format(
                    player.Name,
                    tostring(errorValue)
                ))
                return Result.ok(PlayerData.new(player, self.gamePersistence))
            end)
    end)

    return playerDataResult:map(function(playerData)
        playerData:connectToSaveRequest(function(jsonData)
            return Result.new(function()
                self.datastore:SetAsync(id, jsonData)
            end):mapError(Error.setRequestFailed)
        end)
        return playerData
    end)
end

local PlayerPersistenceMetatable = { __index = PlayerPersistence }

return {
    new = function(datastore, gamePersistence)
        return setmetatable({
            datastore = datastore,
            gamePersistence = gamePersistence,
        }, PlayerPersistenceMetatable)
    end,
}
