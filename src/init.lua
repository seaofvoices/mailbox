return function(_Modules, _ClientModules, Services)
    local module = {}
    local private = {
        playerSaves = {},
        handlers = {},
        playerDataLoaded = {},
    }

    local utils = require(script:WaitForChild('utils'))
    local ErrorKind = utils.ErrorKind
    local GameData = require(script:WaitForChild('GameData'))
    local GamePersistence = require(script:WaitForChild('GamePersistence'))
    local PlayerPersistence = require(script:WaitForChild('PlayerPersistence'))

    local gameData = GameData.new()

    function module.Open(gameDataStoreName, playerSaveStoreName)
        assert(not private.datas, 'Mailbox is already opened! Make sure to call Mailbox.Open(...) only once per game.')

        private.gameStore = Services.DataStoreService:GetDataStore(gameDataStoreName)
        private.gamePersistence = GamePersistence.new(private.gameStore, gameData)

        private.playerStore = Services.DataStoreService:GetDataStore(playerSaveStoreName)
        private.playerPersistence = PlayerPersistence.new(private.playerStore, private.gamePersistence)
        private.datas = {}

        game.Close:Connect(function()
            while next(private.datas) ~= nil do
                wait(1)
            end
        end)
    end

    function module.RegisterDataHandler(dataName, dataLoadedCallback, dataRemovedCallback, getDefaultDataCallback)
        assert(type(dataName) == 'string', '<dataName> has to be a string')
        assert(type(dataLoadedCallback) == 'function', '<dataLoadedCallback> has to be a function')
        assert(type(dataRemovedCallback) == 'function', '<dataRemovedCallback> has to be a function')
        assert(type(getDefaultDataCallback) == 'function', '<getDefaultDataCallback> has to be a function')
        assert(
            not gameData:hasHandler(dataName),
            ('Data named %s has already been registered with a handler'):format(dataName)
        )

        gameData:addHandler(
            dataName,
            getDefaultDataCallback,
            dataLoadedCallback,
            dataRemovedCallback
        )
    end

    function module.OnPlayerReady(player)
        if not private.datas then
            error(
                'Mailbox was not opened before first player was ready.'
                .. ' Make sure to call Mailbox.Open(...) in the `Init` or'
                .. ' `Start` function of a module.'
            )
        end

        local result = private.playerPersistence:load(player)

        while result:isError() do
            local errorValue = result:unwrapError()

            if not errorValue:is(ErrorKind.GetRequestFailed) then
                warn(('cannot retry with error: %s'):format(tostring(errorValue)))
                return
            end

            wait(1)
            if not player.Parent then
                -- player left so Mailbox stops trying to the data
                return
            end
            result = private.playerPersistence:load(player)
        end

        local playerData = result:expect('player data should be loaded')

        local loadResult = playerData:loadLatestGame()

        if loadResult:isOk() then
            private.datas[player] = playerData
        else
            warn(('failed to load data for player %q: %s'):format(
                player.Name,
                tostring(loadResult:unwrapError())
            ))
        end
    end

    function module.OnPlayerLeaving(player)
        if not private.datas then
            return
        end

        local playerData = private.datas[player]
        if not playerData then
            return
        end

        private.datas[player] = nil

        if not Services.RunService:IsStudio() then
            private.SavePlayer(player, playerData)
        end

        private.gamePersistence:remove(player)
    end

    function private.SavePlayer(player, playerData)
        local sessionGameId = playerData:getSessionGameId()
        local gameIdResult = private.gamePersistence:save(player, sessionGameId)

        while gameIdResult:isError() do
            wait(1)
            gameIdResult = private.gamePersistence:save(player, sessionGameId)
        end

        local gameId = gameIdResult:expect('gameId should exist')

        if sessionGameId == nil then
            local result = playerData:saveGame(gameId)

            while result:isError() do
                wait(1)
                result = playerData:saveGame(gameId)
            end
        end
    end

    return module, private
end
