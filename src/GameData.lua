local Result = require(script.Parent:WaitForChild('Result'))

local GameData = {}

function GameData:addHandler(name, getDefault, onLoaded, onRemoved)
    assert(self.handlers[name] == nil, ('handler %q already registered'):format(name))
    self.handlers[name] = {
        getDefault = getDefault,
        onLoaded = onLoaded,
        onRemoved = onRemoved,
    }
end

function GameData:hasHandler(name)
    return self.handlers[name] ~= nil
end

function GameData:create(data)
    if data == nil then
        data = {}
    end
    for name, handler in pairs(self.handlers) do
        local handlerData = data[name]

        if handlerData == nil then
            handlerData = handler.getDefault()
            data[name] = handlerData
        end
    end
    return data
end

function GameData:loadPlayer(player, data)
    for name, handler in pairs(self.handlers) do
        local handlerData = data[name]

        if handlerData == nil then
            handlerData = handler.getDefault()
            data[name] = handlerData
        end

        coroutine.wrap(function()
            local result = Result.new(
                handler.onLoaded,
                player,
                handlerData
            )
            if result:isError() then
                warn(('failed to load data with handler %q (player %s): %s'):format(
                    name,
                    player.Name,
                    tostring(result:unwrapError())
                ))
            end
        end)()
    end
end

function GameData:removePlayer(player)
    for name, handler in pairs(self.handlers) do
        coroutine.wrap(function()
            local result = Result.new(
                handler.onRemoved,
                player
            )
            if result:isError() then
                warn(('failed to remove data with handler %q (player %s): %s'):format(
                    name,
                    player.Name,
                    tostring(result:unwrapError())
                ))
            end
        end)()
    end
end

local GameDataMetatable = { __index = GameData }

return {
    new = function()
        return setmetatable({
            handlers = {},
        }, GameDataMetatable)
    end,
}
