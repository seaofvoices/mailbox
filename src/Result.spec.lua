return function()
    local Result = require(script.Parent.Result)

    describe('new', function()
        it('creates an ok result with the return value of the callback', function()
            local value = {}
            local result = Result.new(function()
                return value
            end)
            expect(result:unwrap()).to.equal(value)
        end)

        it('creates a error result if the callback throws', function()
            local errorMessage = 'some error'
            local result = Result.new(function()
                error(errorMessage)
            end)
            local resultError = result:unwrapError()
            expect(resultError).to.be.a('string')
            expect(resultError:find(errorMessage, 1, true)).to.be.ok()
        end)

        it('returns the callback return value if it is a result', function()
            local innerResult = Result.ok('foo')
            local result = Result.new(function()
                return innerResult
            end)
            expect(result).to.equal(innerResult)
        end)

        it('calls the callback with the given arguments', function()
            local a = 'foo'
            local b = {}
            local c = 7
            local function callback(...)
                return {...}
            end
            local result = Result.new(callback, a, b, c)
            local value = result:unwrap()
            expect(value).to.be.a('table')
            expect(value[1]).to.equal(a)
            expect(value[2]).to.equal(b)
            expect(value[3]).to.equal(c)
        end)
    end)

    describe('ok', function()
        it('creates an ok result', function()
            expect(Result.ok('foo')).to.be.ok()
        end)
    end)

    describe('error', function()
        it('creates an error result', function()
            expect(Result.error('foo')).to.be.ok()
        end)
    end)

    describe('map', function()
        it('creates a new ok result with the mapped value if the result is ok', function()
            local initialResult = Result.ok(1)
            local result = initialResult:map(function(value)
                return value + 1
            end)
            expect(result).never.to.equal(initialResult)
            expect(result:unwrap()).to.equal(2)
        end)

        it('returns a new error result if the result is not ok', function()
            local errorValue = { error = 'foo' }
            local errorResult = Result.error(errorValue)
            local result = errorResult:map(function()
                error('this should not be called!')
            end)
            expect(result).never.to.equal(errorResult)
            expect(result:unwrapError()).to.equal(errorValue)
        end)
    end)

    describe('mapError', function()
        it('creates a new ok result with the same value if the result is ok', function()
            local value = { 'foo' }
            local okResult = Result.ok(value)
            local result = okResult:mapError(function()
                error('this should not be called!')
            end)
            expect(result).never.to.equal(okResult)
            expect(result:unwrap()).to.equal(value)
        end)

        it('returns a new error result with the mapped error if the result is not ok', function()
            local initialResult = Result.error(1)
            local result = initialResult:mapError(function(value)
                return value + 1
            end)
            expect(result).never.to.equal(initialResult)
            expect(result:unwrapError()).to.equal(2)
        end)
    end)

    describe('andThen', function()
        it('returns a new error result if the result is not ok', function()
            local errorValue = {}
            local initialResult = Result.error(errorValue)
            local result = initialResult:andThen(function()
                error('this should not be called!')
            end)
            expect(result).never.to.equal(initialResult)
            expect(result:unwrapError()).to.equal(errorValue)
        end)

        it('returns a new ok result if the function succeed', function()
            local value = { 'foo' }
            local initialResult = Result.ok(value)
            local result = initialResult:andThen(function(valuePassed)
                return valuePassed == value
            end)
            expect(result:isOk()).to.equal(true)
            expect(result:unwrap()).to.equal(true)
        end)

        it('returns an error result if the function throws given an ok result', function()
            local errorMessage = 'oof'
            local initialResult = Result.ok({})
            local result = initialResult:andThen(function()
                error(errorMessage)
            end)
            expect(result:isError()).to.equal(true)
            local resultError = result:unwrapError()
            expect(resultError).to.be.a('string')
            expect(resultError:find(errorMessage, 1, true)).to.be.ok()
        end)

        it('returns the result returned by the function given an ok result', function()
            local expectResult = Result.ok({})
            local result = Result.ok({}):andThen(function()
                return expectResult
            end)
            expect(result).to.equal(expectResult)
        end)
    end)

    describe('orElse', function()
        it('returns a new ok result if the result is ok', function()
            local value = {}
            local initialResult = Result.ok(value)
            local result = initialResult:orElse(function()
                error('this should not be called!')
            end)
            expect(result).never.to.equal(initialResult)
            expect(result:unwrap()).to.equal(value)
        end)

        it('returns an ok result if the function succeed given an error result', function()
            local errorValue = { 'foo' }
            local initialResult = Result.error(errorValue)
            local result = initialResult:orElse(function(valuePassed)
                return valuePassed == errorValue
            end)
            expect(result:isOk()).to.equal(true)
            expect(result:unwrap()).to.equal(true)
        end)

        it('returns an error result if the function throws given an error result', function()
            local errorMessage = 'oof'
            local initialResult = Result.error({})
            local result = initialResult:orElse(function()
                error(errorMessage)
            end)
            expect(result:isError()).to.equal(true)
            local resultError = result:unwrapError()
            expect(resultError).to.be.a('string')
            expect(resultError:find(errorMessage, 1, true)).to.be.ok()
        end)

        it('returns the result returned by the function given an error result', function()
            local expectResult = Result.error({})
            local result = Result.error({}):orElse(function()
                return expectResult
            end)
            expect(result).to.equal(expectResult)
        end)
    end)

    describe('isOk', function()
        it('is true for an ok result', function()
            expect(Result.ok(true):isOk()).to.equal(true)
        end)

        it('is false for an error result', function()
            expect(Result.error('message'):isOk()).to.equal(false)
        end)
    end)

    describe('isError', function()
        it('is true for an error result', function()
            expect(Result.error('message'):isError()).to.equal(true)
        end)

        it('is false for an ok result', function()
            expect(Result.ok(true):isError()).to.equal(false)
        end)
    end)

    describe('expect', function()
        it('returns the value if the result is ok', function()
            local value = {}
            local result = Result.ok(value)
            expect(result:expect('some error message')).to.equal(value)
        end)

        it('throws the given error if the result is not ok', function()
            local errorMessage = 'foo bar'
            expect(function()
                Result.error('error value'):expect(errorMessage)
            end).to.throw(errorMessage)
        end)
    end)

    describe('unwrap', function()
        it('returns the value if the result is ok', function()
            local value = {}
            local result = Result.ok(value)
            expect(result:unwrap()).to.equal(value)
        end)

        it('throws an error if the result is not ok', function()
            local result = Result.error('message')
            expect(function()
                result:unwrap()
            end).to.throw('attempt to unwrap result but found error: message')
        end)
    end)

    describe('unwrapError', function()
        it('returns the error value if the result is not ok', function()
            local value = {}
            local result = Result.error(value)
            expect(result:unwrapError()).to.equal(value)
        end)

        it('throws an error if the result is ok', function()
            local result = Result.ok(8)
            expect(function()
                result:unwrapError()
            end).to.throw('attempt to unwrap error but found value: 8')
        end)
    end)
end
