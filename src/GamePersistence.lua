local HttpService = game:GetService('HttpService')

local utils = require(script.Parent:WaitForChild('utils'))
local fromJson = utils.fromJson
local toJson = utils.toJson
local Error = utils.Error
local Result = require(script.Parent:WaitForChild('Result'))

local GamePersistence = {}

function GamePersistence:save(player, gameId)
    if gameId == nil then
        gameId = self:_getGameId()
    end
    local value = self.loaded[player]
    assert(value, ('player %s has not loaded a game'):format(player.Name))

    return toJson(value)
        :andThen(function(jsonData)
            self.datastore:SetAsync(gameId, jsonData)
        end)
        :map(function()
            return gameId
        end)
end

function GamePersistence:create(player)
    self:_setupData(player)
end

function GamePersistence:load(gameId, player)
    local getResult = Result.new(function()
        return self.datastore:GetAsync(gameId)
    end):mapError(Error.getRequestFailed)

    local dataResult = getResult:andThen(function(jsonValue)
        if jsonValue == nil then
            return Result.error(Error.gameDataNotFound(gameId))
        end

        return fromJson(jsonValue)
            :andThen(function(decodedValue)
                if typeof(decodedValue) ~= 'table' then
                    return Result.error(('attempt to load corupted data: %s'):format(
                        jsonValue
                    ))
                else
                    return Result.ok(decodedValue)
                end
            end)
    end)

    return dataResult:map(function(data)
        self:_setupData(player, data)
    end)
end

function GamePersistence:hasGameLoaded(player)
    return self.loaded[player] ~= nil
end

function GamePersistence:remove(player)
    local data = self.loaded[player]
    if data == nil then
        return
    end
    self.gameData:removePlayer(player)
    self.loaded[player] = nil
end

function GamePersistence:_setupData(player, data)
    data = self.gameData:create(data)
    self.gameData:loadPlayer(player, data)
    self.loaded[player] = data
end

function GamePersistence._getGameId(_self, _player)
    return HttpService:GenerateGUID(false)
end

local GamePersistenceMetatable = { __index = GamePersistence }

return {
    new = function(datastore, gameData)
        return setmetatable({
            datastore = datastore,
            gameData = gameData,
            loaded = {},
        }, GamePersistenceMetatable)
    end,
}
