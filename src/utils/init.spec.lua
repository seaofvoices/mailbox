return function()
    local utils = require(script.Parent)

    local expectFunctions = {
        'createStrictProxy',
        'toJson',
        'fromJson',
    }

    local expectTables = {
        'Error',
        'ErrorKind',
    }

    for _, functionName in ipairs(expectFunctions) do
        it(('has the function %s'):format(functionName), function()
            expect(utils[functionName]).to.be.a('function')
        end)
    end

    for _, name in ipairs(expectTables) do
        it(('has the function %s'):format(name), function()
            expect(utils[name]).to.be.a('table')
        end)
    end
end
