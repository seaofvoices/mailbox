return function()
    local toJson = require(script.Parent.toJson)
    local ErrorKind = require(script.Parent.ErrorKind)

    it('return a result with the json value', function()
        local result = toJson({ foo = 'bar' })
        expect(result).to.be.ok()

        if result:isError() then
            error('toJson failed with message: ' .. tostring(result:unwrapError()))
        end

        expect(result:isOk()).to.equal(true)
        expect(result:unwrap()).to.be.a('string')
    end)

    it('return an error result given a value that cannot be converted', function()
        local result = toJson(function() end)
        expect(result).to.be.ok()

        if result:isOk() then
            error('toJson succeed to convert invalid value')
        end

        expect(result:isError()).to.equal(true)
        expect(result:unwrapError():is(ErrorKind.FailedToEncodeJson)).to.equal(true)
    end)
end
