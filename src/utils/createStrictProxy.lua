local function createStrict(name, table, recursive, metatable)
    assert(typeof(name) == 'string', 'name must be a string')
    assert(typeof(table) == 'table', 'table must be a table')

    local function invalidMember(_self, key)
        local message = ('%q (%s) is not a valid member of %s'):format(
            tostring(key),
            typeof(key),
            name
        )

        error(message, 3)
    end

    if recursive then
        local proxy = {}

        for key, value in pairs(table) do
            if typeof(value) == 'table' then
                proxy[key] = createStrict(('%s.%s'):format(name, key), value, true, getmetatable(value))
            else
                proxy[key] = value
            end
        end

        table = proxy
    end

    local newMetatable = {
        __index = function(self, index)
            local field = table[index]

            if field == nil then
                invalidMember(self, index)
            end

            return field
        end,
        __newindex = invalidMember,
    }

    for metaMethod, value in pairs(metatable or {}) do
        newMetatable[metaMethod] = value
    end

    return setmetatable({}, newMetatable)
end

return createStrict
