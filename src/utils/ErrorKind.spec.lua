return function()
    local ErrorKind = require(script.Parent.ErrorKind)

    it('throws when indexing an unkown error kind', function()
        local function shouldThrow()
            return ErrorKind.foo
        end
        expect(shouldThrow).to.throw('"foo" (string) is not a valid member of ErrorKind')
    end)
end
