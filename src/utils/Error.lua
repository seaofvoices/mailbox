local ErrorKind = require(script.Parent:WaitForChild('ErrorKind'))
local createStrictProxy = require(script.Parent:WaitForChild('createStrictProxy'))

local Error = {}

function Error:is(kind)
    return self.kind == kind
end

local ErrorMetatable = {
    __index = Error,
    __tostring = function(errorObject)
        return ('%s (%s)'):format(
            errorObject.message,
            tostring(errorObject.kind)
        )
    end,
}

local function new(kind, message)
    return setmetatable({
        kind = kind,
        message = message,
    }, ErrorMetatable)
end

return createStrictProxy('Error', {
    getRequestFailed = function(message)
        return new(ErrorKind.GetRequestFailed, message)
    end,
    setRequestFailed = function(message)
        return new(ErrorKind.SetRequestFailed, message)
    end,
    gameDataNotFound = function(gameId)
        return new(
            ErrorKind.GameDataNotFound,
            ('unable to find game data %q'):format(gameId)
        )
    end,
    cannotDecodeJson = function(message)
        return new(ErrorKind.FailedToDecodeJson, message)
    end,
    cannotEncodeJson = function(message)
        return new(ErrorKind.FailedToEncodeJson, message)
    end,
})
