return {
    createStrictProxy = require(script:WaitForChild('createStrictProxy')),
    fromJson = require(script:WaitForChild('fromJson')),
    toJson = require(script:WaitForChild('toJson')),
    Error = require(script:WaitForChild('Error')),
    ErrorKind = require(script:WaitForChild('ErrorKind')),
}
