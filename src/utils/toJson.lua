local HttpService = game:GetService('HttpService')

local Error = require(script.Parent:WaitForChild('Error'))
local Mailbox = script.Parent.Parent
local Result = require(Mailbox:WaitForChild('Result'))

local NULL_RESULT = "null"

do
    local success, result = pcall(function()
        return HttpService:JSONEncode(function() end)
    end)

    if success then
        NULL_RESULT = result
    end
end

return function(value)
    local encodeResult = Result.new(HttpService.JSONEncode, HttpService, value)

    if value ~= nil then
        encodeResult = encodeResult:andThen(function(json)
            if json == NULL_RESULT then
                return Result.error(('unable to convert value %q (of type %s)'):format(
                    tostring(value),
                    typeof(value)
                ))
            end
            return Result.ok(json)
        end)
    end

    return encodeResult:mapError(Error.cannotEncodeJson)
end
