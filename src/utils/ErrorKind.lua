local createStrictProxy = require(script.Parent:WaitForChild('createStrictProxy'))

local function newSymbol(name)
    local symbol = newproxy(true)
    local stringValue = ('ErrorKind.%s'):format(name)
    local metatable = getmetatable(symbol)

    metatable.__tostring = function()
        return stringValue
    end

    return symbol
end

local kinds = {
    'GetRequestFailed',
    'SetRequestFailed',
    'FailedToDecodeJson',
    'FailedToEncodeJson',
    'GameDataNotFound',
}

local kindEnum = {}

for i = 1, #kinds do
    local kind = kinds[i]
    assert(typeof(kind) == 'string', 'error kind name must be a string')
    assert(kindEnum[kind] == nil, ('cannot have two identical ErrorKind %s'):format(kind))
    kindEnum[kind] = newSymbol(kind)
end

return createStrictProxy('ErrorKind', kindEnum)
