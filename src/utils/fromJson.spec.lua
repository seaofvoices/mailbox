return function()
    local fromJson = require(script.Parent.fromJson)
    local ErrorKind = require(script.Parent.ErrorKind)

    it('returns ok result with the value given valid json', function()
        local result = fromJson('{ "foo":"bar" }')

        if result:isError() then
            error('fromJson failed with error: ' .. tostring(result:unwrapError()))
        end

        expect(result:isOk()).to.equal(true)
        local value = result:unwrap()
        expect(value).to.be.a('table')
        expect(value.foo).to.equal('bar')
    end)

    it('return error result given invalid json', function()
        local result = fromJson('{ foo')

        expect(result:isError()).to.equal(true)
        expect(result:unwrapError():is(ErrorKind.FailedToDecodeJson)).to.equal(true)
    end)
end
