local HttpService = game:GetService('HttpService')

local Error = require(script.Parent:WaitForChild('Error'))
local Mailbox = script.Parent.Parent
local Result = require(Mailbox:WaitForChild('Result'))

return function(string)
    return Result.new(HttpService.JSONDecode, HttpService, string)
        :mapError(Error.cannotDecodeJson)
end
