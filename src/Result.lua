local Result = {}
local ResultMetatable = { __index = Result }

local Api = {}

local function isResult(value)
    if typeof(value) ~= 'table' then
        return false
    end

    local metatable = getmetatable(value)

    if not metatable then
        return false
    end

    return metatable == ResultMetatable
end

function Result:map(callback)
    if self.success then
        return Api.ok(callback(self.value))
    else
        return Api.error(self.errorValue)
    end
end

function Result:mapError(callback)
    if self.success then
        return Api.ok(self.value)
    else
        return Api.error(callback(self.errorValue))
    end
end

function Result:andThen(callback)
    if self.success then
        return Api.new(callback, self.value)
    else
        return Api.error(self.errorValue)
    end
end

function Result:orElse(callback)
    if self.success then
        return Api.ok(self.value)
    else
        return Api.new(callback, self.errorValue)
    end
end

function Result:isOk()
    return self.success
end

function Result:isError()
    return not self.success
end

function Result:expect(message)
    if self.success then
        return self.value
    else
        error(message)
    end
end

function Result:unwrap()
    if self.success then
        return self.value
    else
        error(('attempt to unwrap result but found error: %s'):format(
            tostring(self.errorValue)
        ))
    end
end

function Result:unwrapError()
    if self.success then
        error(('attempt to unwrap error but found value: %s'):format(
            tostring(self.value)
        ))
    else
        return self.errorValue
    end
end

local function create(success, value, errorValue)
    return setmetatable({
        success = success,
        value = value,
        errorValue = errorValue,
    }, ResultMetatable)
end

function Api.new(callback, ...)
    local success, result = pcall(callback, ...)
    local value = nil
    local errorValue = nil
    if success then
        if isResult(result) then
            return result
        end

        value = result
    else
        errorValue = result
    end

    return create(success, value, errorValue)
end

function Api.ok(value)
    return create(true, value, nil)
end

function Api.error(errorValue)
    return create(false, nil, errorValue)
end

return Api
