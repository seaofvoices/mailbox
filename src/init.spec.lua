return function()
    local loader = require(script.Parent)

    local HttpService = game:GetService('HttpService')
    local RunService = game:GetService('RunService')

    local ANY_FUNC = function() end

    local function createDatastoreMock(options)
        options = options or {}
        local data = options.data or {}

        return {
            data = data,
            GetAsync = options.GetAsync or function(_self, key)
                return data[key]
            end,
            SetAsync = options.SetAsync or function(_self, key, value)
                data[key] = value
            end,
        }
    end

    local function newMailbox(options)
        options = options or {}
        options.gameDatastore = options.gameDatastore or createDatastoreMock()
        options.playerDatastore = options.playerDatastore or createDatastoreMock()
        if options.open == nil then
            options.open = true
        end

        local gameDatastore = 'gamestore'
        local playerDatastore = 'playerstore'

        local mailbox, private = loader({}, {}, {
            DataStoreService = {
                GetDataStore = function(_self, name)
                    if name == gameDatastore then
                        return options.gameDatastore
                    elseif name == playerDatastore then
                        return options.playerDatastore
                    else
                        return nil
                    end
                end
            },
            HttpService = HttpService,
            RunService = options.RunService or RunService,
        })

        for method, callback in pairs(options.private or {}) do
            private[method] = callback
        end

        if options.open then
            mailbox.Open(gameDatastore, playerDatastore)
        end

        return mailbox, private
    end

    local function createHandlerMock(name)
        local handler = {
            name = name,
            playerLoaded = {},
        }

        function handler.onLoaded(player, data)
            assert(handler.playerLoaded[player] == nil, 'attempt to load player twice')
            handler.playerLoaded[player] = data
        end

        function handler.getDefault()
            return { value = name }
        end

        function handler.onRemoved(player)
            assert(handler.playerLoaded[player] ~= nil, 'attempt to remove unknown player')
            handler.playerLoaded[player] = nil
        end

        return handler
    end

    local function createPlayerMock()
        return {
            UserId = 123,
            Name = 'jeparlefrancais',
        }
    end

    describe('Open', function()
        it('throws when calling `Open` twice', function()
            local mailbox = newMailbox({ open = false })

            local function callOpen()
                mailbox.Open('otherstore', 'teststore')
            end

            expect(callOpen).never.to.throw()
            expect(callOpen).to.throw()
        end)

        it('connects to game Close event', function()
            local mailbox = newMailbox({ open = false })
            local connected = nil
            local environment = getfenv(mailbox.Open)
            environment.game = {
                Close = {
                    Connect = function(_, callback)
                        connected = callback
                    end
                },
            }
            mailbox.Open('foo', 'bar')
            expect(connected).to.be.a('function')
        end)
    end)

    describe('RegisterDataHandler', function()
        it('throws when data name is not a string', function()
            local mailbox = newMailbox({ open = false })
            local function shouldThrow()
                mailbox.RegisterDataHandler({}, ANY_FUNC, ANY_FUNC, ANY_FUNC)
            end
            expect(shouldThrow).to.throw('<dataName> has to be a string')
        end)

        it('throws when loaded callback is not a function', function()
            local mailbox = newMailbox({ open = false })
            local function shouldThrow()
                mailbox.RegisterDataHandler('foo', nil, ANY_FUNC, ANY_FUNC)
            end
            expect(shouldThrow).to.throw('<dataLoadedCallback> has to be a function')
        end)

        it('throws when removed callback is not a function', function()
            local mailbox = newMailbox({ open = false })
            local function shouldThrow()
                mailbox.RegisterDataHandler('foo', ANY_FUNC, nil, ANY_FUNC)
            end
            expect(shouldThrow).to.throw('<dataRemovedCallback> has to be a function')
        end)

        it('throws when get default callback is not a function', function()
            local mailbox = newMailbox({ open = false })
            local function shouldThrow()
                mailbox.RegisterDataHandler('foo', ANY_FUNC, ANY_FUNC, nil)
            end
            expect(shouldThrow).to.throw('<getDefaultDataCallback> has to be a function')
        end)

        it('throws when the same name is used twice', function()
            local mailbox = newMailbox({ open = false })
            local function register()
                mailbox.RegisterDataHandler(
                    'foo',
                    ANY_FUNC,
                    ANY_FUNC,
                    ANY_FUNC
                )
            end
            register()
            expect(register).to.throw('Data named foo has already been registered with a handler')
        end)
    end)

    describe('OnPlayerReady', function()
        it('throws if mailbox is not open before first player is processed', function()
            local mailbox = newMailbox({ open = false })

            local function shouldThrow()
                mailbox.OnPlayerReady(createPlayerMock())
            end

            expect(shouldThrow).to.throw(
                'Mailbox was not opened before first player was ready.'
                .. ' Make sure to call Mailbox.Open(...) in the `Init` or'
                .. ' `Start` function of a module.'
            )
        end)

        describe('new player', function()
            it('calls each handler with their default data', function()
                local mailbox = newMailbox()
                local fooHandler = createHandlerMock('foo')
                local barHandler = createHandlerMock('bar')
                mailbox.RegisterDataHandler(
                    fooHandler.name,
                    fooHandler.onLoaded,
                    fooHandler.onRemoved,
                    fooHandler.getDefault
                )
                mailbox.RegisterDataHandler(
                    barHandler.name,
                    barHandler.onLoaded,
                    barHandler.onRemoved,
                    barHandler.getDefault
                )
                local player = createPlayerMock()

                mailbox.OnPlayerReady(player)

                expect(fooHandler.playerLoaded[player]).to.be.ok()
                expect(barHandler.playerLoaded[player]).to.be.ok()

                expect(fooHandler.playerLoaded[player].value).to.equal('foo')
                expect(barHandler.playerLoaded[player].value).to.equal('bar')
            end)
        end)

        describe('player coming back', function()
            it('gets the default data for missing handlers', function()
                local fooHandler = createHandlerMock('foo')
                local barHandler = createHandlerMock('bar')

                local player = createPlayerMock()
                local playerStore = createDatastoreMock()
                local gameStore = createDatastoreMock()
                local gameId = '0'
                playerStore.data[tostring(player.UserId)] = HttpService:JSONEncode({
                    { DataId = gameId, Time = os.time() - 60 },
                })
                local fooValue = 'not default value'
                gameStore.data[gameId] = HttpService:JSONEncode({
                    [fooHandler.name] = { value = fooValue },
                })
                local mailbox = newMailbox({
                    gameDatastore = gameStore,
                    playerDatastore = playerStore,
                })
                mailbox.RegisterDataHandler(
                    fooHandler.name,
                    fooHandler.onLoaded,
                    fooHandler.onRemoved,
                    fooHandler.getDefault
                )
                mailbox.RegisterDataHandler(
                    barHandler.name,
                    barHandler.onLoaded,
                    barHandler.onRemoved,
                    barHandler.getDefault
                )

                mailbox.OnPlayerReady(player)

                expect(fooHandler.playerLoaded[player]).to.be.ok()
                expect(barHandler.playerLoaded[player]).to.be.ok()

                expect(fooHandler.playerLoaded[player].value).to.equal(fooValue)
                expect(barHandler.playerLoaded[player].value).to.equal('bar')
            end)
        end)
    end)

    describe('OnPlayerLeaving', function()
        it('saves the game data and player data', function()
            local player = createPlayerMock()
            local playerStore = createDatastoreMock()
            local gameStore = createDatastoreMock()

            local mailbox = newMailbox({
                gameDatastore = gameStore,
                playerDatastore = playerStore,
                RunService = {
                    IsStudio = function()
                        return false
                    end,
                }
            })
            local fooHandler = createHandlerMock('foo')
            local barHandler = createHandlerMock('bar')
            mailbox.RegisterDataHandler(
                fooHandler.name,
                fooHandler.onLoaded,
                fooHandler.onRemoved,
                fooHandler.getDefault
            )
            mailbox.RegisterDataHandler(
                barHandler.name,
                barHandler.onLoaded,
                barHandler.onRemoved,
                barHandler.getDefault
            )
            mailbox.OnPlayerReady(player)

            mailbox.OnPlayerLeaving(player)

            local playerSavedData = playerStore.data[tostring(player.UserId)]
            expect(playerSavedData).to.be.a('string')

            local decodedPlayerData = HttpService:JSONDecode(playerSavedData)
            expect(#decodedPlayerData).to.equal(1)
            expect(decodedPlayerData[1].Time).to.be.a('number')
            expect(decodedPlayerData[1].DataId).to.be.a('string')

            local gameId = decodedPlayerData[1].DataId

            local gameSavedData = gameStore.data[gameId]
            expect(gameSavedData).to.be.a('string')
            local gameData = HttpService:JSONDecode(gameSavedData)
            expect(gameData[fooHandler.name]).to.be.ok()
            expect(gameData[fooHandler.name].value).to.equal('foo')
            expect(gameData[barHandler.name]).to.be.ok()
            expect(gameData[barHandler.name].value).to.equal('bar')
        end)

        it('calls each handler to remove the data', function()
            local mailbox = newMailbox()
            local fooHandler = createHandlerMock('foo')
            local barHandler = createHandlerMock('bar')
            mailbox.RegisterDataHandler(
                fooHandler.name,
                fooHandler.onLoaded,
                fooHandler.onRemoved,
                fooHandler.getDefault
            )
            mailbox.RegisterDataHandler(
                barHandler.name,
                barHandler.onLoaded,
                barHandler.onRemoved,
                barHandler.getDefault
            )
            local player = createPlayerMock()

            mailbox.OnPlayerReady(player)

            expect(fooHandler.playerLoaded[player]).to.be.ok()
            expect(barHandler.playerLoaded[player]).to.be.ok()

            mailbox.OnPlayerLeaving(player)

            expect(fooHandler.playerLoaded[player]).never.to.be.ok()
            expect(barHandler.playerLoaded[player]).never.to.be.ok()
        end)

        it('does nothing if mailbox is not opened', function()
            local mailbox = newMailbox({ open = false })
            local player = createPlayerMock()

            expect(function()
                mailbox.OnPlayerLeaving(player)
            end).never.to.throw()
        end)
    end)
end
