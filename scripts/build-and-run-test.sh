#!/bin/sh

set -ex

rojo build -o test-place.rbxlx test-place.project.json
run-in-roblox --place test-place.rbxlx --script ./scripts/run-tests.lua
